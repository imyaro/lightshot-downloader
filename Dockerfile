FROM python:3

RUN pip install beautifulsoup4

COPY main.py /opt

WORKDIR /opt

CMD ["python", "/opt/main.py"]
