#!/usr/bin/env python3
import logging
import http.server
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen

log = logging.getLogger('logging')
log.setLevel(logging.DEBUG)

formatter = logging.Formatter('...> %(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh = logging.FileHandler('logs')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

stdout = logging.StreamHandler()
stdout.setLevel(logging.DEBUG)
stdout.setFormatter(formatter)

log.addHandler(stdout)
log.addHandler(fh)

class convert_handler(http.server.BaseHTTPRequestHandler):
    def do_GET(request):
        path = request.path
        if path == '/favicon.ico':
            request.send_response(404, 'No favicon, gerara here')
            request.wfile.flush()
            return

        log.info('request path: ' + path)

        headers = {'User-Agent': 'Mozilla/5.0'}
        image_request = Request('https://prnt.sc/' + path, headers=headers)

        html = urlopen(image_request).read()

        bs = BeautifulSoup(html, 'html.parser')
        image = bs.find('img', id='screenshot-image')['src']

        image_request = Request('https:' + image, headers=headers)
        res = urlopen(image_request)
        data = res.read()

        request.send_response(200)
        request.send_header('Content-type', res.headers['Content-Type'])
        request.send_header('Content-Length', res.headers['Content-Length'])
        request.end_headers()

        request.wfile.write(data)
        request.wfile.flush()

def run_server():
    log.info(' Start server')

    host = '0.0.0.0'
    port = 3009
    httpd = http.server.HTTPServer((host, port), convert_handler)
    httpd.serve_forever()

def make_simple_response(request, text, code):
    request.send_response(code)
    request.end_headers()
    request.wfile.write(text.encode('utf-8'))
    request.wfile.flush()

def error_response(request, text):
    output = '{"error": "' + text + '"}'
    make_simple_response(request, output, 400)

if __name__ == "__main__":
    run_server()
